#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007
struct Point{int x, y;};

// GCD AND LCM
    int gcd (int a, int b) { return b ? gcd (b, a % b) : a; }
    int lcm (int a, int b) { return a / gcd(a, b) * b; }
 
// MODULAR EXPONENTIATION
    int powmod(int x,int y) 
    { 
        if (y == 0) return 1; 
        int p = powmod(x,y/2)%mod; 
        p = (p*p)%mod;   
        return (y%2 == 0)?p:(x*p)%mod; 
    }
 
// MODULAR INVERSE
    int inverse(int a) 
    { 
       return powmod(a,mod-2); 
    }

//Binomial Coefficients
    const int limbin = 500001;
    int fact[limbin], factinv[limbin] , coefficientflag = 0;
    void Util_nCr()
    {
        fact[0] = 1 , factinv[0] = 1;
        for(int i = 1 ; i < limbin ; i++)
        {
            fact[i] = (fact[i-1] * i)%mod;
            factinv[i] = (factinv[i-1] * inverse(i))%mod;
        }
    }
    int nCr(int n , int r)
    {
        if(!coefficientflag) coefficientflag = 1 , Util_nCr();
        if(n<r) return 0;             
        return ((fact[n] * (factinv[n-r]) % mod * (factinv[r]))%mod) ;
    }

// STRING HASHING
    int hprime = 31 , hmod = 1000000009;
    int stringhash(string s)
    {
        int res = 0;
        for(auto i : s)
            res = (res*hprime%hmod + (i-'a'))%hmod;
        return res;
    }
    vector<int> hashoflen(string s, int k)
    {
        int l = s.length();
        vector<int> ans(l-k + 1);
        ans[0] = stringhash(s.substr(0,k));
        int hell = 1 , tm = k - 1;
        while(tm--) hell = (hell*hprime)%hmod;
        for(int i = k ; i < l ; i++)
            ans[i-k+1] = ((ans[i-k] - hell*(s[i-k]-'a')%hmod + hmod)%hmod*hprime%hmod + (s[i]-'a'))%mod;
        return ans;
    }
    vector<int> prefixhash(string s)
    {
        int l = s.length();
        vector<int> hash(l+1);
        hash[0] = 0;
        for(int i = 0 ; i < l ; i++)
            hash[i+1] = (hash[i]*hprime%hmod + (s[i]-'a'))%hmod;
        return hash;
    }
    vector<int> suffixhash(string s)
    {
        int l = s.length();
        vector<int> hash(l+1);
        hash[0] = 0;
        int pprime = 1;
        for(int i = 0 ; i < l ; i++)
            hash[i+1] = (hash[i] + (s[l-i-1]-'a')*pprime%hmod)%hmod , pprime %= hmod;;
        return hash;
    }


// SUM OF FIRST N POWERS (STARTING FROM ONE)
    int sumofpowers(int p, int n)
    {
        if(powmod(p,n+1)==1 || (p-1+mod)%mod==0)
            return n+1;
        return (((powmod(p,n+1)-1+mod)%mod)*inverse((p-1+mod)%mod))%mod;
    }

// SUM OF FIRST N TERMS OF AP MODULAR
    int sumofap(int a , int d , int n)
    {
        return n*(2*a + (n-1)*d)%mod*inverse(2)%mod;
    }

// SUM OF FIRST N TERMS OF GP 
    int sumofgp(int a , int r , int n)
    {
        return a*(powmod(r,n)%mod-1+mod)%mod*inverse((r-1+mod)%mod)%mod;
    }

// AREA OF POLYGON
    double polygonArea(double X[], double Y[], int n) 
    { 
        double area = 0.0; 
        int j = n - 1; 
        for (int i = 0; i < n; i++) 
        { 
            area += (X[j] + X[i]) * (Y[j] - Y[i]); 
            j = i;
        } 
        return abs(area / 2.0); 
    }


// PRIMES UPTO N
    vector<int> getprimes(int n)
    {
        vector<int> primes;
        bool vis[n+1] = {};
        for(int i = 2 ; i <= n ; i++)
        {
            if(vis[i])
                continue;
            primes.push_back(i);
            for(int j = i ; j <= n ; j+=i)
                vis[j] = 1;
        }
        return primes;
    }

// PRIME TEST
    bool isPrime(int x) 
    {
        for (int d = 2; d * d <= x; d++) 
            if (x % d == 0)
                return false;
        return true;
    }

// EULER TOTIENT FUNCTION 
    int phi(int n) 
    {
        int result = n;
        for (int i = 2; i * i <= n; i++) 
            if (n % i == 0) 
            {
                while (n % i == 0)
                    n /= i;
                result -= result / i;
            }
        if (n > 1)
            result -= result / n;
        return result;
    }

// EULER TOTIENT FUNCTION FROM 1 TO N
    vector<int> phi_1_to_n(int n) 
    {
        vector<int> phi(n + 1);
        phi[0] = 0;
        phi[1] = 1;
        for (int i = 2; i <= n; i++)
            phi[i] = i - 1;

        for (int i = 2; i <= n; i++)
            for (int j = 2 * i; j <= n; j += i)
                  phi[j] -= phi[i];
        return phi;
    }

//LONGEST INCREASING SUBSEQUENCE
    int lis(vector<int> const& a) 
    {
        int n = a.size();
        const int INF = 1e9;
        vector<int> d(n+1, INF);
        d[0] = -INF;
        for (int i = 0; i < n; i++) 
        {
            int j = upper_bound(d.begin(), d.end(), a[i]) - d.begin();
            if (d[j-1] < a[i] && a[i] < d[j])
                d[j] = a[i];
        }
        int ans = 0;
        for (int i = 0; i <= n; i++)
            if (d[i] < INF)
                ans = i;
        return ans;
    }

//LONGEST COMMON SUBSEQUENCE
    int lcs(string X, string Y, int m, int n )  
    {  
        int L[m + 1][n + 1];      
        for(int i = 0; i <= m; i++)  
            for(int j = 0; j <= n; j++)  
                if (i == 0 || j == 0)  
                    L[i][j] = 0;  
                else if (X[i - 1] == Y[j - 1])  
                    L[i][j] = L[i - 1][j - 1] + 1;
                else
                    L[i][j] = max(L[i - 1][j], L[i][j - 1]);  
        return L[m][n];  
    }  
    
// FFT OF MANY VECTORS USING DIVIDE AND CONQUER 
    const double PI = acos(-1); 
    const int fftmod = 998244353;
    const int generator = 3;
    const int LN = 20;
    const int root_pw = (1<<LN);
    const int upperlimit = 1000100;
    vector<vector<int>> v(1000);
    #define root powmod(generator,(fftmod-1)/root_pw)
    #define root_1 inverse(root)
    void fft (vector<int> & a, bool invert) 
    {
        int n = a.size();
        for (int i=1, j=0; i<n; ++i) {
            int bit = n >> 1;
            for (; j>=bit; bit>>=1)
                j -= bit;
            j += bit;
            if (i < j)
                swap (a[i], a[j]);
        }
        for (int len=2; len<=n; len<<=1) {
            int wlen = invert ? root_1 : root;
            for (int i=len; i<root_pw; i<<=1)
                wlen = (wlen * 1ll * wlen % fftmod);
            for (int i=0; i<n; i+=len) {
                int w = 1;
                for (int j=0; j<len/2; ++j) {
                    int u = a[i+j],  v = (a[i+j+len/2] * 1ll * w % fftmod);
                    a[i+j] = u+v < fftmod ? u+v : u+v-fftmod;
                    a[i+j+len/2] = u-v >= 0 ? u-v : u-v+fftmod;
                    w = (w * 1ll * wlen % fftmod);
                }
            }
        }
        if (invert) {
            int nrev = inverse(n);
            for (int i=0; i<n; ++i)
                a[i] = (a[i] * 1ll * nrev % fftmod);
        }
    }

    vector<int> multiply(vector<int> a,vector<int> b){
        int n1=a.size();
        int n2=b.size();
        int gg=n1+n2-1;
        int m=1;
        while(m<gg)m<<=1;
        a.resize(m);
        b.resize(m);
        fft(a,false);
        fft(b,false);
        vector<int> c(m);
        for(int i=0;i<m;i++){
            c[i]=(1LL*a[i]*b[i])%fftmod;
        }
        fft(c,true);
        c.resize(gg);
        return c;
    }

    vector<int> mul(int l , int r)
    {
        if(l==r)
            return v[l];
        else if(l==r-1)
            return multiply(v[l],v[r]);
        int mid = (l+r)/2;
        return multiply(mul(mid+1,r),mul(l,mid));
    }



// PERMUTATION OF PRIME FACTORS OF N
    map<int,int> m;
    struct primeFactorization { 
        int countOfPf, primeFactor; 
    }; 
    int cur = 0;
    int res = 1;
    map<int,int> prim;
    void generateDivisors(int curIndex, int curDivisor, vector<primeFactorization>& arr) 
    { 
        if (curIndex == arr.size()) 
        {
            m[curDivisor] = res;
            return; 
        } 
        int pcur = cur;
        int pres = res;
        map<int,int> temp = prim;
        for (int i = 0; i <= arr[curIndex].countOfPf; ++i) 
        { 
            generateDivisors(curIndex + 1, curDivisor, arr); 
            prim[arr[curIndex].primeFactor]++;
            cur++;
            res *= cur;
            res %= mod;
            res *= inverse(prim[arr[curIndex].primeFactor]);
            res%=mod;
            curDivisor *= arr[curIndex].primeFactor; 
        }
        res = pres;
        prim = temp;
        cur = pcur;
    } 
    void findDivisors(int n) 
    { 
        vector<primeFactorization> arr; 
        for (int i = 2; i * i <= n; ++i) { 
            if (n % i == 0) { 
                int count = 0; 
                while (n % i == 0) { 
                    n /= i; 
                    count += 1; 
                } 
                arr.push_back({ count, i }); 
            } 
        } 
        if (n > 1) 
            arr.push_back({ 1, n });  
        int curIndex = 0, curDivisor = 1; 
        generateDivisors(curIndex, curDivisor, arr); 
    } 



// PICK'S THEORAM
// We denote its area by S, number of integral coordinates strictly inside by I 
// and the number of points lying on polygon sides by B then [ S = I + B/2 - 1 ] 


// SEGMENT SIEVE O(Nlog(B-A))
    #define A 1000000000000LL
    #define B 1000000100000LL
    bitset<B-A> p;
    void seive()
    {
        p.set();
        for(int i=2;i*i<=B;i++)
            for(int j=((A+i-1)/i)*i;j<=B;j+=i)
                p.reset(j-A);
    }

/* Returns orientation of point C wrt line from B to A
    -1 : C to left
     1 : C to right
     0 : C on line
*/
    int ccw(Point a, Point b, Point c)
    {
        int ans = (b.x-a.x)*(c.y-a.y) - (b.y-a.y)*(c.x-a.x);
        return ans < 0 ? -1 : ans > 0;
    }

// SEGMENT TREE (SUM)
    int seg[500000],a[100000];
    void build(int l, int r, int i)
    {
        if(l>r){return;}
        if(l==r){seg[i] = a[l-1];return;}
        build((l+r)/2+1,r,2*i+1);
        build(l,(l+r)/2,2*i);
        seg[i] = seg[2*i] + seg[2*i+1];
    }

    void upd(int i , int v , int ind , int l , int r)
    {
        seg[ind] += v;
        if(l==r) return;
        int mid = (l+r)/2;
        if(mid>=i) upd(i,v,2*ind,l,mid);
        else upd(i,v,2*ind+1,mid+1,r);
    }

    int cal(int l, int r, int ind , int lef, int rig)
    {
        if(l==lef && r==rig) return seg[ind];
        int mid = (lef+rig)/2;
        int res = 0;
        if(lef<=l && l<=mid) res += cal(l,min(r,mid),2*ind,lef,mid);
        if(mid<r) res += cal(max(mid+1,l),r,2*ind+1,mid+1,rig);
        return res;
    }

// NUMBER OF SOLUTIONS OF X_1+X_2+X_3...X_k = N
    int partitionNIntoK(int k , int n)
    {
        return nCr(n+k-1,k);
    }

//Split String
    vector<string> split(string s, char delimiter)
    {
        // std::string delimiter = ">=";
        vector<string> v;
        size_t pos = 0;
        std::string token;
        while ((pos = s.find(delimiter)) != std::string::npos) 
        {
            token = s.substr(0, pos);
            v.push_back(token);
            s.erase(0, pos + 1);
        }
        v.push_back(s);
        return v;
    } 

int main()
{

}
